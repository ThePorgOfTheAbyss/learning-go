package main

import (
	"fmt"
	"math/rand"

	"bitbucket.org/ThePorgOfTheAbyss/learning-go/car"
)

func main() {

	// var test string
	// var test int

	/*
		a := util.Addint(3)

		fmt.Println(util.Printthing("gabe"))
		fmt.Println(a)

		dog := animal.NewAnimal("dog", 50, 36)
		fmt.Println(dog)
		unicorn := unicorn.NewUnicorn("Red", "jeff")
		fmt.Println(unicorn)
	*/

	carInstance := car.NewCar("Chevy", 500000, "Red")

	test := []car.Car{carInstance, carInstance, carInstance, carInstance}

	for x, entry := range test {
		// can omit value with _, by omitting x, for loop will only run for as long as the array
		entry.Price = 100000.0 * rand.Float64()

		test[x] = entry
	}

	fmt.Println(test[3:])
	//colon before 3 is up to 3, after three is after 3
}
