package unicorn

import "encoding/json"

//
type Unicorn struct {
	Color string
	Name  string
}

//
func NewUnicorn(color string, name string) Unicorn {
	return Unicorn{
		Color: color,
		Name:  name,
	}

}
func (unicorn Unicorn) String() string {
	marshaledVal, _ := json.MarshalIndent(unicorn, "", "  ") // Marshal JSON

	return string(marshaledVal) // Return JSON
}
