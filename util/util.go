package util

import "fmt"

// Printthing - thing that prints a string q
func Printthing(q string) string {
	return fmt.Sprintf("hello: %s", q)
}

// Addint - adds 1 to int i
func Addint(i int) int {
	return i + 1
}
