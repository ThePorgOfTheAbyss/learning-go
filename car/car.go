package car

import "encoding/json"

// Car - car metadata container
type Car struct {
	Brand string
	Price float64
	Color string
}

// NewCar - construct new car
func NewCar(brand string, price float64, color string) Car {
	return Car{
		Brand: brand,
		Price: price,
		Color: color,
	}
}

func (car Car) String() string {
	marshaledVal, _ := json.MarshalIndent(car, "", "  ") // Marshal JSON

	return string(marshaledVal) // Return JSON
}

//turtles are euewuygudnduygsugusguuuuguguususuygssu
func (car Car) Bytes() []byte {
	marshaledVal, _ := json.MarshalIndent(car, "", "  ") // Marshal JSON

	return marshaledVal
}
