package animal

import "encoding/json"

// Animal - container holding animal meta
type Animal struct {
	Species string
	Height  int
	Weight  int
}

// NewAnimal - construct new animal
func NewAnimal(species string, height int, weight int) Animal {
	return Animal{
		Species: species,
		Height:  height,
		Weight:  weight,
	}
}

//the blue string is indicating that there is a string method to print from
func (animal Animal) Bytes() []byte {
	marshaledVal, _ := json.MarshalIndent(animal, "", "  ")

	return (marshaledVal)
}
